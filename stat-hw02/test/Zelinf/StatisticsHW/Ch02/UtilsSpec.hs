{-# LANGUAGE OverloadedLists #-}

module Zelinf.StatisticsHW.Ch02.UtilsSpec
  ( spec
  )
where

import           Data.Vector                    (Vector)
import qualified Data.Vector                    as V
import           Test.Hspec

import           Zelinf.StatisticsHW.Ch02.Utils (groupBy, squareSum)

spec :: Spec
spec = do
  describe "groupBy" $ do
    it "splits list according to key" $
      groupBy id (V.fromList [1, 2, 3, 2, 3, 4])
        `shouldBe` [[1], [2, 2], [3, 3], [4]]
    it "returns empty list on empty list" $
      groupBy id (V.fromList []) `shouldBe` ([] :: [Vector Int])
  describe "squareSum" $ do
    it "squares elements then sum them together" $ do
      squareSum ([] :: [Int]) `shouldBe` 0
      squareSum ([1..3] :: [Int]) `shouldBe` 14
      squareSum ([3, 1, 2] :: [Int]) `shouldBe` 14
