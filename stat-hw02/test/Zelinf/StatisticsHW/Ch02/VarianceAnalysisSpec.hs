{-# LANGUAGE OverloadedLists #-}

module Zelinf.StatisticsHW.Ch02.VarianceAnalysisSpec
  ( spec
  ) where

import           Test.Hspec

import           Zelinf.StatisticsHW.Ch02.VarianceAnalysis

spec :: Spec
spec = describe "anovaOf" $ do
  it "must gives the same result as a book example" $ do
    let sampleData =
          [ (1, [0.236, 0.238, 0.248, 0.245, 0.243])
          , (2, [0.257, 0.253, 0.255, 0.254, 0.261])
          , (3, [0.258, 0.264, 0.259, 0.267, 0.262])
          ]
        (anova1, r) = anovaOf sampleData
    r `shouldBe` True
    (anova1_fBound anova1, 3.89) `shouldSatisfy` approxEqual 0.01
    (anova1_fRatio anova1, 32.92) `shouldSatisfy` approxEqual 0.01

approxEqual :: (Fractional a, Ord a) => a -> (a, a) -> Bool
approxEqual r (x, y) = abs (x - y) < r
