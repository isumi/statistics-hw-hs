{-# LANGUAGE OverloadedStrings #-}

module Zelinf.StatisticsHW.Ch02.MedicalInfo
  ( Sex(..)
  , MedicalInfo(..)
  )
where

import           Control.Monad
import           Data.Csv
import           Data.Text     (Text)

data MedicalInfo = MedicalInfo
  { miAge      :: Int
  , miSex      :: Sex
  , miBMI      :: Double
  , miChildren :: Int
  , miIsSmoker :: Bool
  , miRegion   :: Text
  , miCharges  :: Double
  } deriving (Eq, Show)

instance FromRecord MedicalInfo where
  parseRecord v
    | length v == 7 = MedicalInfo
      <$> v .! 0
      <*> v .! 1
      <*> v .! 2
      <*> v .! 3
      <*> (fmap unIsSmoker $ v .! 4)
      <*> v .! 5
      <*> v .! 6
    | otherwise = mzero

data Sex = Male | Female
  deriving (Show, Eq)

instance FromField Sex where
  parseField s
    | s == "male"   = pure Male
    | s == "female" = pure Female

newtype IsSmoker = IsSmoker
  { unIsSmoker :: Bool
  } deriving (Show, Eq)

instance FromField IsSmoker where
  parseField s = pure (IsSmoker (s == "yes"))
