module Zelinf.StatisticsHW.Ch02.LinearRegress
  ( predicateNext
  , olsRegress'
  , applyEquation
  , OLSRegressResult(..)
  ) where

import           Data.Either                      (fromRight)
import qualified Data.Matrix                      as DMat
import qualified Data.Vector.Unboxed              as UV
import           Statistics.Distribution          (findRoot)
import           Statistics.Distribution.StudentT (studentT)
import           Statistics.Matrix                (Matrix, Vector)
import qualified Statistics.Matrix                as Mat
import           Statistics.Regression            (olsRegress)
import           Zelinf.StatisticsHW.Ch02.Utils   (transposeV)

olsRegress' :: [Vector] -- ^ predictor vectors
            -> Vector -- ^ responder vector
            -> OLSRegressResult
olsRegress' preds resp =
  let (eq, rSquare) = olsRegress preds resp
      yHats = UV.fromList $ fmap (applyEquation eq) (transposeV preds)
      mseValue = mse yHats resp (length preds)
   in OLSRegressResult eq rSquare mseValue

data OLSRegressResult = OLSRegressResult
  { _olsRegressResult_equation :: Vector
  , _olsRegressResult_rSquare  :: Double
  , _olsRegressResult_mseValue :: Double
  }

applyEquation :: Vector -- ^ equation (of length n)
              -> Vector -- ^ x values (of length n - 1)
              -> Double -- ^ result
applyEquation equ xs = sum $ zipWith (*) (UV.toList equ) (UV.toList xs ++ [1])

predicateNext :: [Vector] -- ^ predictor vectors (must be non-empty)
              -> Double -- ^ MSE
              -> Vector -- ^ new input (must be of the same length as the list of predicator vectors)
              -> Double -- ^ y hat of the new input
              -> Double -- ^ confidence (1-α)
              -> (Double, Double) -- ^ confidence interval of the new output
predicateNext preds mseValue x yHat confidence = (yHat - halfWidth, yHat + halfWidth)
  where
    n = UV.length (head preds)
    p = length preds + 1
    tDist = studentT (fromIntegral (n - p))
    x0 = Mat.fromColumns [UV.cons 1 x]
    predMat = Mat.fromColumns $ (UV.replicate n 1) : preds
    z = head . Mat.toList $ (Mat.transpose x0)
      `Mat.multiply` (matInverse (Mat.transpose predMat `Mat.multiply` predMat))
      `Mat.multiply` x0
    halfWidth = findRoot tDist ((1 + confidence) / 2) 2 (-5) 200
      * sqrt (mseValue * (1 + z))

-- | Compute error sum of squares
sse :: Vector -- ^ y hats
    -> Vector -- ^ ys
    -> Double
sse yHats ys = UV.sum $ UV.zipWith (\yh y -> (yh - y)^2) yHats ys

-- | Compute mean squared error (MSE)
mse :: Vector -- ^ y hats
    -> Vector -- ^ ys
    -> Int -- degree of freedom
    -> Double
mse yHats ys = mseFromSSE (sse yHats ys) (UV.length ys)

mseFromSSE :: Double -- ^ sse
           -> Int -- ^ sample size
           -> Int -- ^ degree of freedom
           -> Double
mseFromSSE sse' n dof = sse' / fromIntegral (n - dof - 1)

matInverse :: Matrix -> Matrix
matInverse = flip withDMat (fromRight (DMat.zero 1 1) . DMat.inverse)

withDMat :: Matrix -> (DMat.Matrix Double -> DMat.Matrix Double) -> Matrix
withDMat mat f = dMatToMatrix . f . matrixToDMat $ mat

dMatToMatrix :: (DMat.Matrix Double) -> Matrix
dMatToMatrix = Mat.fromRowLists . DMat.toLists

matrixToDMat :: Matrix -> (DMat.Matrix Double)
matrixToDMat = DMat.fromLists . Mat.toRowLists
