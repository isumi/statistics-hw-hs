module Zelinf.StatisticsHW.Ch02.Solution4
  ( main
  ) where

import qualified Data.Vector                               as V
import qualified Data.Vector.Unboxed                       as UV
import           System.Environment                        (getArgs)

import           Zelinf.StatisticsHW.Ch02.MedicalInfo
import           Zelinf.StatisticsHW.Ch02.Utils            (listToMapCombDup, readMedicalInfoFromArgs,
                                                            sexToDouble)
import           Zelinf.StatisticsHW.Ch02.VarianceAnalysis

main :: IO ()
main = getArgs >>= mainWithArgs

mainWithArgs :: [String] -> IO ()
mainWithArgs args = do
  records <- readMedicalInfoFromArgs args
  let result = anova2SexSmokingCharges records
  putStrLn $ "F_A的值和边界依次是：" <> show (anova2_fa result)
  putStrLn $ "F_B的值和边界依次是：" <> show (anova2_fb result)
  putStrLn $ "F_AB的值和边界依次是：" <> show (anova2_fab result)
  putStrLn $ "性别对医疗费用的影响：" <> (infoFromBool . uncurry (>) . anova2_fa) result
  putStrLn $ "是否吸烟对医疗费用的影响：" <> (infoFromBool . uncurry (>) . anova2_fb) result
  putStrLn $ "性别和是否吸烟的共同作用对医疗费用的影响：" <> (infoFromBool . uncurry (>) . anova2_fab) result

infoFromBool :: Bool -> String
infoFromBool True  = "是"
infoFromBool False = "否"

anova2SexSmokingCharges :: V.Vector MedicalInfo
                        -> Anova2
anova2SexSmokingCharges records = anovaOf2 0.95 theMap
  where
    theMap = fmap UV.fromList . listToMapCombDup . V.toList
      . fmap (\mi -> ((sexToDouble (miSex mi), boolToDouble (miIsSmoker mi)), miCharges mi))
      $ records

boolToDouble :: Bool -> Double
boolToDouble True = 1.0
boolToDouble _    = 0.0
