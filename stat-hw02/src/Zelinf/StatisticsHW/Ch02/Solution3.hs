module Zelinf.StatisticsHW.Ch02.Solution3
  ( main
  ) where

import qualified Data.ByteString.Lazy                      as LBS
import           Data.Csv
import qualified Data.Vector                               as V
import qualified Data.Vector.Unboxed                       as UV
import           System.Environment                        (getArgs)
import           Zelinf.StatisticsHW.Ch02.MedicalInfo
import           Zelinf.StatisticsHW.Ch02.Utils            (listToMapCombDup, readMedicalInfoFromArgs,
                                                            sexToDouble)
import           Zelinf.StatisticsHW.Ch02.VarianceAnalysis

main :: IO ()
main = getArgs >>= mainWithArgs

mainWithArgs :: [String] -> IO ()
mainWithArgs args = (readMedicalInfoFromArgs args) >>= (putStrLn . showAnova1Result . anovaSexCharges)

{-|
  Computes whether sex has an impaction on charges or not.
-}
anovaSexCharges :: V.Vector MedicalInfo -- ^some medical infos
                -> (Anova1, Bool) -- ^True if such impaction is found
anovaSexCharges = anovaOf . fmap UV.fromList . listToMapCombDup
  . V.toList . fmap (\mi -> (sexToDouble (miSex mi), miCharges mi))

showAnova1Result :: (Anova1, Bool) -> String
showAnova1Result (anova1, impact) =
    "F ratio is " <> show (anova1_fRatio anova1) <> "\n"
 <> "Boundary is " <> show (anova1_fBound anova1) <> "\n"
 <> "Thus, sex " <> (if impact then "has" else "doesn't have") <> " an impaction on charges\n"
