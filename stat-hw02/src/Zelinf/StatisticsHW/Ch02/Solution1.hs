{-# LANGUAGE FlexibleContexts    #-}
{-# LANGUAGE OverloadedLists     #-}
{-# LANGUAGE ScopedTypeVariables #-}

module Zelinf.StatisticsHW.Ch02.Solution1
  ( main
  ) where

import qualified Data.ByteString.Lazy                   as LBS
import           Data.Csv
import           Data.Foldable                          (traverse_)
import qualified Data.Vector                            as V
import qualified Data.Vector.Generic                    as GV
import qualified Data.Vector.Unboxed                    as UV
import           Statistics.Matrix                      (Vector)
import           Statistics.Regression                  (olsRegress)
import           System.Environment                     (getArgs)
import           Zelinf.StatisticsHW.Ch02.LinearRegress
import           Zelinf.StatisticsHW.Ch02.MedicalInfo
import           Zelinf.StatisticsHW.Ch02.Utils         (readMedicalInfoFromArgs,
                                                         transposeV)

main :: IO ()
main = getArgs >>= mainWithArgs

mainWithArgs :: [String] -> IO ()
mainWithArgs args = do
  records :: V.Vector MedicalInfo <- readMedicalInfoFromArgs args
  let ageV = GV.convert $ fmap (fromIntegral . miAge) records
      bmiV = GV.convert $ fmap miBMI records
      childrenV = GV.convert $ fmap (fromIntegral . miChildren) records
      chargeV = GV.convert $ fmap miCharges records
      [ageV', bmiV', childrenV', chargeV'] :: [Vector] = fmap takeUntilLast5 [ageV, bmiV, childrenV, chargeV]
      OLSRegressResult regEq rSquare mseValue = olsRegress' [ageV', bmiV', childrenV'] chargeV'
  putStrLn $
    "医疗费用 = " <> (show . (UV.! 0)) regEq <> " * 年龄 + "
    <> (show . (UV.! 1)) regEq <> " * BMI + "
    <> (show . (UV.! 2)) regEq <> " * 孩子数量 + "
    <> (show . (UV.! 3)) regEq
  putStrLn $ "R^2 = " <> show rSquare
  let toPredXs :: [Vector] = transposeV (fmap dropUntilLast5 [ageV, bmiV, childrenV])
  putStrLn "最后5条数据的预测值和95%置信度的置信区间依次是："
  putStrLn "------------------------"
  flip traverse_ toPredXs $ \xs -> do
    let yHat = applyEquation regEq xs
        (lowerBound, upperBound) = predicateNext [ageV', bmiV', childrenV'] mseValue xs yHat 0.95
    putStrLn $ "预测值：" <> show yHat
    putStrLn $ "置信区间：(" <> show lowerBound <> ", " <> show upperBound <> ")"
    putStrLn "------------------------"

dropUntilLast5 :: Vector -> Vector
dropUntilLast5 v = UV.drop (UV.length v - 5) v

takeUntilLast5 :: Vector -> Vector
takeUntilLast5 v = UV.take (UV.length v - 5) v
