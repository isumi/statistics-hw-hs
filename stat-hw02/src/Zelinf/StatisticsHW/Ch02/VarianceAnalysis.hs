{-# LANGUAGE ScopedTypeVariables #-}

module Zelinf.StatisticsHW.Ch02.VarianceAnalysis
  ( anovaOf
  , Anova1(..)
  , anovaOf2
  , Anova2(..)
  )
where

import           Data.Foldable                         (foldl')
import           Data.Map.Strict                       (Map)
import qualified Data.Map.Strict                       as Map
import           Data.Set                              (Set)
import qualified Data.Set                              as Set
import qualified Data.Vector                           as V
import qualified Data.Vector.Generic                   as GV
import qualified Data.Vector.Unboxed                   as UV
import           Statistics.Distribution               (findRoot)
import           Statistics.Distribution.FDistribution (fDistribution)
import           Statistics.Matrix                     (Vector)

import           Zelinf.StatisticsHW.Ch02.MedicalInfo
import           Zelinf.StatisticsHW.Ch02.Utils        (squareSum, sumVector)

anovaOf :: Map Double Vector -> (Anova1, Bool)
anovaOf ax =
  let (sums :: Map Double (Double, Int)) =
        fmap (\xs -> ((sum . UV.toList) xs, UV.length xs)) ax
      totalSum = sum (fmap fst sums)
      s = Map.size ax
      n = sum . fmap UV.length $ ax
      st = (sum . fmap (squareSum . UV.toList) $ ax)
           - (totalSum^2 / fromIntegral n)
      sa = (sum . fmap (\(t, nj) -> t^2 / fromIntegral nj) $ sums)
           - (totalSum^2 / fromIntegral n)
      se = st - sa
      sa_avg = sa / fromIntegral (s - 1)
      se_avg = se / fromIntegral (n - s)
      f_ratio = sa_avg / se_avg
      fDist = fDistribution (s - 1) (n - s)
      fBound = findRoot fDist 0.95 2.5 1 1000
      r = fBound < f_ratio
   in (Anova1 s n st sa se sa_avg se_avg f_ratio fBound, r)

data Anova1 = Anova1
  { anova1_s      :: Int
  , anova1_n      :: Int
  , anova1_st     :: Double
  , anova1_sa     :: Double
  , anova1_se     :: Double
  , anova1_saAvg  :: Double
  , anova1_seAvg  :: Double
  , anova1_fRatio :: Double
  , anova1_fBound :: Double
  } deriving (Show, Eq)

anovaOf2 :: Double -> Map (Double, Double) Vector -> Anova2
anovaOf2 confidence abx =
  let abx' = truncateVectorsToSameLength abx
      aKeys = occurrencesInKeys fst abx'
      bKeys = occurrencesInKeys snd abx'
      r = Set.size aKeys
      s = Set.size bKeys
      aKeys' = Set.toList aKeys
      bKeys' = Set.toList bKeys
      t = UV.length . head . Map.elems $ abx'
      errorDoF = r * s * (t - 1)
      tij i j = (fmap sumVector abx') Map.! (i, j)
      ti i = sum $ fmap (\j -> tij i j) bKeys'
      tj j = sum $ fmap (\i -> tij i j) aKeys'
      totalSum = sum $ fmap (\i -> ti i) aKeys'
      z = totalSum ^ 2 / fromIntegral (r * s * t)
      st = (sum $ fmap (squareSum . UV.toList) abx') - z
      sa = (sum $ fmap (\i -> (ti i)^2) aKeys') / fromIntegral (s*t) - z
      sb = (sum $ fmap (\j -> (tj j)^2) bKeys') / fromIntegral (r*t) - z
      sab = (sum $ fmap (\i -> sum $ fmap (\j -> (tij i j)^2) bKeys') aKeys') / fromIntegral t - z - sa - sb
      se = st - sa - sb - sab
      saAvg = sa / fromIntegral (r - 1)
      sbAvg = sb / fromIntegral (s - 1)
      sabAvg = sab / fromIntegral ((r - 1) * (s - 1))
      seAvg = se / fromIntegral errorDoF
      fa = saAvg / seAvg
      fb = sbAvg / seAvg
      fab = sabAvg / seAvg
      faBound = findRootForFDist confidence (r - 1) errorDoF
      fbBound = findRootForFDist confidence (s - 1) errorDoF
      fabBound = findRootForFDist confidence ((r - 1) * (s - 1)) errorDoF
   in Anova2 (fa, faBound) (fb, fbBound) (fab, fabBound)

occurrencesInKeys :: (Ord k, Ord a) => (k -> a) -> Map k v -> Set a
occurrencesInKeys f m = Set.map f . Map.keysSet $ m

data Anova2 = Anova2
  { anova2_fa  :: (Double, Double) -- ^ value and boundary of F_A
  , anova2_fb  :: (Double, Double) -- ^ value and boundary of F_B
  , anova2_fab :: (Double, Double) -- ^ value and boundary of F_(A*B)
  } deriving Show

findRootForFDist :: Double -- ^ confidence
                 -> Int -- ^ arg1 of FDistribution
                 -> Int -- ^ arg2 of FDistribution
                 -> Double
findRootForFDist conf x y = findRoot (fDistribution x y) conf 2.5 1 1000

minVecLength :: Foldable t => t Vector -> Int
minVecLength = foldl' (\minLen v -> if UV.length v < minLen then UV.length v else minLen) maxBound

truncateVectorsToSameLength :: (Functor t, Foldable t) => t Vector -> t Vector
truncateVectorsToSameLength vs = fmap (UV.take minLen) vs
  where minLen = minVecLength vs
