module Zelinf.StatisticsHW.Ch02.Utils
  ( groupBy
  , listToMapCombDup
  , squareSum
  , readMedicalInfoFromArgs
  , transposeV
  , sumVector
  , sexToDouble
  )
where

import qualified Data.ByteString.Lazy                 as LBS
import           Data.Csv                             (HasHeader (..), decode)
import           Data.Foldable                        (foldl')
import qualified Data.List                            as List
import           Data.Map.Strict                      (Map)
import qualified Data.Map.Strict                      as Map
import qualified Data.Vector                          as V
import qualified Data.Vector.Generic                  as GV
import qualified Data.Vector.Unboxed                  as UV
import           Statistics.Matrix                    (Vector)
import           System.Exit                          (exitFailure)
import           Zelinf.StatisticsHW.Ch02.MedicalInfo

groupBy :: (GV.Vector v a, Ord b) => (a -> b) -> v a -> [v a]
groupBy f =
  fmap (GV.fromList . snd)
    . Map.toList
    . foldl' acc Map.empty
    . GV.toList
  where acc m a = Map.insertWith (++) (f a) [a] m

listToMapCombDup :: Ord k => [(k, v)] -> Map k [v]
listToMapCombDup = foldl' acc Map.empty
  where acc m (k, v) = Map.insertWith (++) k [v] m

squareSum :: (Foldable t, Num a) => t a -> a
squareSum = foldl' (\y x -> y + x^2) 0

readMedicalInfoFromArgs :: [String] -> IO (V.Vector MedicalInfo)
readMedicalInfoFromArgs [dataFile] = do
  csvData <- LBS.readFile dataFile
  case decode HasHeader csvData of
    Left errMsg   -> do
      putStrLn errMsg
      exitFailure
      pure mempty
    Right records -> pure records

transposeV :: [Vector] -> [Vector]
transposeV vs = fmap (\i -> UV.fromList (fmap (UV.! i) vs)) [0.. vLen - 1]
  where vLen = if null vs then 0 else UV.length (head vs)

sumVector :: Vector -> Double
sumVector = sum . UV.toList

sexToDouble :: Sex -> Double
sexToDouble Male   = 1.0
sexToDouble Female = 0.0
