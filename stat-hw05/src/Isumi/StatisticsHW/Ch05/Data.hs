{-# LANGUAGE FlexibleContexts    #-}
{-# LANGUAGE OverloadedLists     #-}
{-# LANGUAGE OverloadedStrings   #-}
{-# LANGUAGE ScopedTypeVariables #-}

module Isumi.StatisticsHW.Ch05.Data
  ( readTrainData
  , readTestData
  , eraseVecAt
  ) where

import           Control.DeepSeq        (force)
import           Control.Monad.Except
import           Control.Monad.IO.Class (liftIO)
import qualified Data.ByteString.Lazy   as LBS
import qualified Data.Csv               as Csv
import           Data.IntMap.Strict     (IntMap)
import qualified Data.IntMap.Strict     as IntMap
import           Data.Map.Strict        (Map)
import qualified Data.Map.Strict        as Map
import           Data.Text              (Text)
import qualified Data.Text              as Text
import qualified Data.Vector            as V
import qualified Data.Vector.Storable   as SV
import           GHC.Exts               (fromList)
import qualified Numeric.LinearAlgebra  as LA

readTrainData :: FilePath -> ExceptT Text IO [(LA.Vector Double, Int)]
readTrainData filepath = do
  records :: [LA.Vector Double] <- readTestData filepath
  pure $ fmap splitLabel records

splitLabel :: LA.Vector Double -> (LA.Vector Double, Int)
splitLabel v =
  let theData = LA.subVector 0 (len - 1) v
      label = LA.atIndex v (len - 1)
   in (theData, round label)
  where
    len = LA.size v

readTestData :: FilePath -> ExceptT Text IO [LA.Vector Double]
readTestData filepath = do
  content <- liftIO $ LBS.readFile filepath
  records <- withExceptT Text.pack $
    liftEither (Csv.decode Csv.NoHeader content)
  case traverse recordToLevels records of
    Nothing       -> throwError "File format is invalid"
    Just records' -> pure $ V.toList records'

recordToLevels :: V.Vector Text -> Maybe (LA.Vector Double)
recordToLevels v =
  fmap (LA.fromList . fmap fromIntegral . V.toList) . sequenceA $
    V.zipWith Map.lookup v fieldLevels

eraseVecAt :: (SV.Storable a, LA.Container LA.Vector a)
           => Int -> LA.Vector a -> LA.Vector a
eraseVecAt i v =
  let v1 = SV.take i v
      v2 = SV.drop (i + 1) v
   in v1 <> v2

fieldLevels :: V.Vector (Map Text Int)
fieldLevels = force $ fmap (fromList . flip zip [0..])
  [ ["low", "med", "high", "vhigh"]
  , ["low", "med", "high", "vhigh"]
  , ["2", "3", "4", "5more"]
  , ["2", "4", "more"]
  , ["small", "med", "big"]
  , ["low", "med", "high"]
  , ["unacc", "acc", "good", "vgood"]
  ]
