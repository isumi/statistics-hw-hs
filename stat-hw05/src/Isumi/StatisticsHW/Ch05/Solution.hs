{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TupleSections       #-}

module Isumi.StatisticsHW.Ch05.Solution
  ( main
  ) where

import           Control.Monad.Except                (ExceptT, runExceptT)
import           Control.Monad.Random.Class
import           Data.Bifunctor                      (first)
import           Data.Foldable                       (minimumBy)
import           Data.Function                       (on)
import           Data.IntMap.Strict                  (IntMap)
import qualified Data.IntMap.Strict                  as IntMap
import           Data.Ord                            (comparing)
import           Data.Text                           (Text)
import qualified Data.Text.IO                        as Text
import qualified Data.Vector                         as V
import           Isumi.StatisticsHW.Ch05.Data
import qualified Isumi.StatisticsHW.Ch05.Mahalanobis as Mah
import qualified Numeric.LinearAlgebra               as LA
import           System.Environment                  (getArgs)
import           System.Exit                         (exitFailure)
import           System.IO                           (stderr)

main :: IO ()
main = do
  [pathTrain, pathTest] <- getArgs

  trainData :: [(LA.Vector Double, Int)] <- exitOnError $ readTrainData pathTrain
  testData :: [LA.Vector Double] <- exitOnError $ readTestData pathTest

  putStrLn "不删除第6个属性："
  runTrainTestAndSelfTest trainData testData

  let trainData' = fmap (first (eraseVecAt 5)) trainData
      testData' = fmap (eraseVecAt 5) testData
  putStrLn "删除第6个属性："
  runTrainTestAndSelfTest trainData' testData'

runTrainTestAndSelfTest :: [(LA.Vector Double, Int)] -> [LA.Vector Double] -> IO ()
runTrainTestAndSelfTest trainData testData = do
  let (ordinaryLabels, meanCovLabels) = trainAndTest trainData testData
  putStrLn "  对测试集的结果："
  putStrLn $ "    假定协方差不同：" ++ show ordinaryLabels
  putStrLn $ "    假定协方差相同：" ++ show meanCovLabels

  (selfTestData, otherTrainData) <- randomlyTake 200 trainData
  let (ordinaryLabels, meanCovLabels) = trainAndTest otherTrainData (fmap fst selfTestData)
  putStrLn $ "  自测准确率："
  putStrLn $ "    假定协方差不同：" ++ show (matchRatio ordinaryLabels (fmap snd selfTestData))
  putStrLn $ "    假定协方差相同：" ++ show (matchRatio meanCovLabels (fmap snd selfTestData))

exitOnError :: ExceptT Text IO a -> IO a
exitOnError m =
  runExceptT m >>= either (\x -> Text.hPutStrLn stderr x >> exitFailure) pure

trainAndTest :: [(LA.Vector Double, Int)] -- ^train data
             -> [LA.Vector Double] -- ^test data
             -> ([Int], [Int]) -- ^labels for test data. (ordinary, mean cov)
trainAndTest trainData testData =
  let mahGroups = fmap Mah.mkGroup (groupTrainData trainData)
      mahGroups' = Mah.avgGroupCov mahGroups
   in ( fmap (flip nearest mahGroups) testData
      , fmap (flip nearest mahGroups') testData
      )

matchRatio :: Eq a => [a] -> [a] -> Double
matchRatio xs ys =
  fromIntegral (length (filter id $ zipWith (==) xs ys)) / fromIntegral len
  where
    len = min (length xs) (length ys)

nearest :: LA.Vector Double -> IntMap Mah.Group -> Int
nearest v groups =
  fst . minimumBy (compare `on` snd) . IntMap.toList $
    Mah.distance v <$> groups

groupTrainData :: [(LA.Vector Double, Int)] -> IntMap [LA.Vector Double]
groupTrainData xs =
  IntMap.unionsWith (++) . fmap (\(v, i) -> IntMap.singleton i [v]) $ xs

randomlyTake :: (Eq a, MonadRandom m) => Int -> [a] -> m ([a], [a])
randomlyTake n [] = pure ([], [])
randomlyTake 0 xs = pure ([], xs)
randomlyTake n xs = do
  i <- getRandomR (0, length xs - 1)
  let (xs1, y:xs2) = splitAt i xs
  (ys, xs') <- randomlyTake (n - 1) (xs1 ++ xs2)
  pure $ (y:ys, xs')
