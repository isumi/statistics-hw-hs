{-# LANGUAGE ScopedTypeVariables #-}

module Isumi.StatisticsHW.Ch05.Mahalanobis
  ( distance
  , Group
  , mkGroup
  , avgGroupCov
  ) where

import           Control.Exception     (assert)
import           Data.Foldable         (toList)
import           Data.Foldable         (foldl')
import qualified Numeric.LinearAlgebra as LA

distance :: LA.Vector Double
         -> Group
         -> Double
distance v (Group mean covMatInv _) =
  let u = LA.asRow (v - mean) :: LA.Matrix Double
      resultMat = u LA.<> covMatInv LA.<> LA.tr' u
   in assert (LA.size resultMat == (1, 1)) (sqrt $ LA.atIndex resultMat (0, 0))

data Group = Group
  { groupMean      :: !(LA.Vector Double)
  , groupCovMatInv :: !(LA.Matrix Double)
  , groupCovMat    :: !(LA.Matrix Double)
  }

mkGroup :: Foldable t
        => t (LA.Vector Double)
        -> Group
mkGroup vs =
  let (mean, covMat) = LA.meanCov . LA.fromRows . toList $ vs
      covMat' = LA.unSym covMat
   in Group mean (LA.inv covMat') covMat'

avgGroupCov :: (Foldable t, Functor t)
            => t Group
            -> t Group
avgGroupCov groups =
  let ms = flip fmap groups $ \(Group _ _ covMat) ->
             let n = fromIntegral $ LA.rows covMat - 1
              in (n, LA.scalar n * covMat)
      newCovMat = sum (fmap snd ms) / LA.scalar (sum . fmap fst $ ms)
   in fmap
        (\g -> g { groupCovMat = newCovMat, groupCovMatInv = LA.inv newCovMat })
        groups
