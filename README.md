# statistics-hw-hs
.stack.yaml

My statistics homework, rewritten in Haskell.

## Configure

**Note**: Starting from hw-04, I switched to cabal new build. Previous packages
will be add to cabal.project eventually. But before that, you have to compile
previous packages using `stack`, new packages using cabal new build. The
instructions of new build won't be listed here, until the migration is done.

Before running `stack`, you need to create a `stack.yaml` file and provide
MATLAB include/link dirs.

Instructions:
```
cp stack.base.yaml stack.yaml
```
Then open `stack.yaml` and add `extra-include-dirs` and `extra-lib-dirs`, for
example:
```yaml
extra-include-dirs:
  [ /home/isumi/opt/MATLAB/R2018b/extern/include
  ]
extra-lib-dirs: 
  [ /home/isumi/opt/MATLAB/R2018b/extern/bin/glnxa64
  ]
```
Change the paths according to your matlab installation location. If you are not
on Linux, consult MathWorks' documentation for platform-specific include/link
directories.

## Compile and run

Having configured the environment, you can now build the project.

Make sure you have latest version of `stack` installed. If not, follow the
instructions at [Stack
documentation](https://docs.haskellstack.org/en/stable/install_and_upgrade/) to
install the Haskell Tool Stack.

This project is split into several cabal packages. You may build all packages at once with:
```sh
stack build
```
Or, specific package:
```sh
stack build stat-hw02
```
Having built the libraries and executables, you can run executables with, for example:
```sh
stack exec stat-hw02-s1 -- stat-hw02/data/data.txt
```

Unit tests can be run with `stack test`:
```
stack test # run all tests
stack test stat-hw02 # run tests for package stat-hw02
```

