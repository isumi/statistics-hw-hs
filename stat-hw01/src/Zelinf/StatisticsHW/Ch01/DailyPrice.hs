module Zelinf.StatisticsHW.Ch01.DailyPrice
  ( DailyPrice
  , fromFoldable
  , fromStockDayInfo
  , intersectByDay
  , stockCode
  , corrOf
  , CorrPval(..)
  )
where

import           Data.Text                      ( Text )
import qualified Data.Map.Strict               as Map
import           Data.Map.Strict                ( Map )
import           Data.Time                      ( Day )
import qualified Data.Vector.Unboxed           as UV
import           Statistics.Correlation         ( pearson
                                                , spearman
                                                )

import           Zelinf.StatisticsHW.Ch01.StockDayInfo
import           Zelinf.StatisticsHW.Ch01.Utils
import           Zelinf.StatisticsHW.Corr       ( pValFromCorr )

-- |Stock's daily price is a map from date to price
data DailyPrice = DailyPrice StockCode (Map Day Double)

fromFoldable :: Foldable t => StockCode -> t (Day, Double) -> DailyPrice
fromFoldable sc = DailyPrice sc . Map.fromList . foldableToList

fromStockDayInfo
  :: (Functor t, Foldable t) => t StockDayInfo -> Maybe DailyPrice
fromStockDayInfo infos =
  let values = fmap (\x -> (unDate $ siDate x, (siHigh x + siLow x) / 2)) infos
      sc     = siCode <$> safeHead infos
  in  (`fromFoldable` values) <$> sc

{-| Get price pairs that has the same day
-}
intersectByDay :: DailyPrice -> DailyPrice -> [(Double, Double)]
intersectByDay (DailyPrice _ m1) (DailyPrice _ m2) =
  fmap snd . Map.toList $ Map.intersectionWith (,) m1 m2

stockCode :: DailyPrice -> StockCode
stockCode (DailyPrice sc _) = sc

corrOf :: DailyPrice -> DailyPrice -> CorrPval
corrOf s1 s2 = CorrPval pearson' pearsonPval spearman' spearmanPval
 where
  vec          = UV.fromList (intersectByDay s1 s2)
  pearson'     = pearson vec
  pearsonPval  = pValFromCorr (UV.length vec) pearson'
  spearman'    = spearman vec
  spearmanPval = pValFromCorr (UV.length vec) spearman'

data CorrPval = CorrPval
  { corrPearson :: Double
  , corrPearsonPval :: Double
  , corrSpearman :: Double
  , corrSpearmanPval :: Double
  } deriving (Show, Eq)
