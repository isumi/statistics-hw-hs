{-# LANGUAGE DeriveGeneric #-}

module Zelinf.StatisticsHW.Ch01.StockDayInfo
  ( StockDayInfo(..)
  , Date(..)
  , StockCode
  )
where

import           Data.Text                      ( Text )
import           Data.Csv
import           GHC.Generics                   ( Generic )
import           Data.Time
import           Data.ByteString               as BS
import           Data.ByteString                ( ByteString )
import           Data.Char                      ( ord )
import           Text.Read                      ( readMaybe )
import qualified Data.ByteString.Char8         as BSC

type StockCode = Text

data StockDayInfo = StockDayInfo
  { siDate :: Date
  , siOpen :: Double
  , siClose :: Double
  , siHigh :: Double
  , siLow :: Double
  , siVolumn :: Double
  , siCode :: StockCode
  } deriving (Generic, Show)

instance FromRecord StockDayInfo

newtype Date = Date
  { unDate :: Day
  } deriving (Show, Eq)

instance FromField Date where
  parseField s =
    let dateSep = fromIntegral (ord '-')
     in
    case BS.split dateSep s of
      parts@[year, month, day] -> case (do
        y' <- readMaybe $ BSC.unpack year
        m' <- readMaybe $ BSC.unpack month
        d' <- readMaybe $ BSC.unpack day
        fromGregorianValid y' m' d')
        of
          Just v -> (pure . Date) v
          Nothing -> fail errMsg
      _ -> fail errMsg
    where errMsg = "Illegal date format"
