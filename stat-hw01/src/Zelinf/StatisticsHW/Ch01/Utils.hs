{-# LANGUAGE TupleSections #-}

module Zelinf.StatisticsHW.Ch01.Utils where

foldableToList :: Foldable t => t a -> [a]
foldableToList = foldr (:) []

combinations :: [a] -> [(a, a)]
combinations xs = comb xs xs
 where
  comb (x : xs) (y : ys') = fmap (x, ) ys' ++ comb xs ys'
  comb []       _         = []
  comb _        []        = []

safeHead :: Foldable t => t a -> Maybe a
safeHead = safeHead' . foldableToList
 where
  safeHead' (x : xs) = Just x
  safeHead' []       = Nothing

invertOrdering :: Ordering -> Ordering
invertOrdering GT = LT
invertOrdering LT = GT
invertOrdering EQ = EQ
