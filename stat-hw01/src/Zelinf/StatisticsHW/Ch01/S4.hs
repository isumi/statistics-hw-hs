{-# LANGUAGE ScopedTypeVariables #-}

module Zelinf.StatisticsHW.Ch01.S4
  ( main
  )
where

import qualified Data.ByteString.Lazy          as BL
import           Data.Csv
import qualified Data.Vector                   as V
import           Data.Vector                    ( Vector )
import           System.Environment             ( getArgs )
import           System.Exit
import           System.Directory               ( listDirectory )
import           System.FilePath                ( takeExtension
                                                , (</>)
                                                )
import           Data.Maybe                     ( fromJust )
import           Data.List                      ( sortBy )
import           Data.Foldable                  ( traverse_ )

import           Zelinf.StatisticsHW.Ch01.StockDayInfo
import           Zelinf.StatisticsHW.Ch01.DailyPrice
import           Zelinf.StatisticsHW.Ch01.Utils ( combinations
                                                , invertOrdering
                                                )
import           Zelinf.StatisticsHW.Corr       ( pValFromCorr )

main :: IO ()
main = do
  putStrLn "Solution 4"
  cmdArgs <- getArgs
  case cmdArgs of
    [dataDir] -> mainWithDataDir dataDir
    _         -> exitFailure

mainWithDataDir :: FilePath -> IO ()
mainWithDataDir dir = do
  files <-
    fmap (dir </>)
    .   filter (\f -> takeExtension f == ".csv")
    <$> listDirectory dir
  (stocks :: [DailyPrice]) <-
    (fmap . fmap) (fromJust . fromStockDayInfo)
      $ traverse readStockDayInfoFromFile files
  let (stockPairs :: [(DailyPrice, DailyPrice)]) = combinations stocks
  let (corrs :: [StockCorrPval]) = zipWith
        (\(s1, s2) c -> ((stockCode s1, stockCode s2), c))
        stockPairs
        (fmap (uncurry corrOf) stockPairs)
  let n            = 5
  let max5Pearson  = maxNPearson n corrs
  let min5Pearson  = minNPearson n corrs
  let max5Spearman = maxNSpearman n corrs
  let min5Spearman = minNSpearman n corrs

  putStrLn "Pearson 相关性最强的5对"
  printCorrPval corrPearson corrPearsonPval max5Pearson
  putStrLn "Pearson 相关性最弱的5对"
  printCorrPval corrPearson corrPearsonPval min5Pearson
  putStrLn "Spearman 相关性最强的5对"
  printCorrPval corrSpearman corrSpearmanPval max5Spearman
  putStrLn "Spearman 相关性最弱的5对"
  printCorrPval corrSpearman corrSpearmanPval min5Spearman

readStockDayInfoFromFile :: FilePath -> IO [StockDayInfo]
readStockDayInfoFromFile csvFile = do
  csvData <- BL.readFile csvFile
  case decode HasHeader csvData of
    Left err -> do
      putStrLn err
      exitFailure
    Right vec -> pure $ V.toList vec

printCorrPval
  :: (CorrPval -> Double) -> (CorrPval -> Double) -> [StockCorrPval] -> IO ()
printCorrPval getCoef getPval = traverse_ $ \((c1, c2), cp) ->
  putStrLn
    $  "股票"
    ++ show c1
    ++ "和股票"
    ++ show c2
    ++ "的相关系数为："
    ++ show (getCoef cp)
    ++ "，p值为："
    ++ show (getPval cp)

type StockCorrPval = ((StockCode, StockCode), CorrPval)

topNBy
  :: Int
  -> (CorrPval -> CorrPval -> Ordering)
  -> [StockCorrPval]
  -> [StockCorrPval]
topNBy n cmp corrs = take n $ sortBy (\(_, c1) (_, c2) -> cmp c1 c2) corrs

maxNPearson :: Int -> [StockCorrPval] -> [StockCorrPval]
maxNPearson n = topNBy n (\c1 c2 -> invertOrdering $ compareByPearson c1 c2)

minNPearson :: Int -> [StockCorrPval] -> [StockCorrPval]
minNPearson n = topNBy n compareByPearson

maxNSpearman :: Int -> [StockCorrPval] -> [StockCorrPval]
maxNSpearman n = topNBy n (\c1 c2 -> invertOrdering $ compareBySpearman c1 c2)

minNSpearman :: Int -> [StockCorrPval] -> [StockCorrPval]
minNSpearman n = topNBy n compareBySpearman

compareByPearson :: CorrPval -> CorrPval -> Ordering
compareByPearson c1 c2 = abs (corrPearson c1) `compare` abs (corrPearson c2)

compareBySpearman :: CorrPval -> CorrPval -> Ordering
compareBySpearman c1 c2 = abs (corrSpearman c1) `compare` abs (corrSpearman c2)
