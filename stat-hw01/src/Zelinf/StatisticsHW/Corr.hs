module Zelinf.StatisticsHW.Corr
  ( pValFromCorr
  )
where

import           Statistics.Distribution
import           Statistics.Distribution.StudentT

{-| Compute pval from correlation coefficient
-}
pValFromCorr
  :: Int -- ^Size of the sample data
  -> Double -- ^pearson or spearman correlation coefficient
  -> Double -- ^the p-val
pValFromCorr n r = 2 * complCumulative (studentT (fromIntegral n - 2)) t0
  where t0 = abs $ r * sqrt (fromIntegral (n - 2) / (1 - r ^ 2))
