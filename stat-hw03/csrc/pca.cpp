#include "pca.h"

#include <MatlabEngine.hpp>
#include <MatlabDataArray.hpp>
#include <memory>
#include <vector>
#include <cstdbool>
#include <cassert>

using namespace matlab::engine;
using namespace matlab::data;

namespace {

void copyMatrixToCArray(const Array &mat, size_t rows, double *cArray) {
    size_t row = 0;
    for (size_t i = 0; i != mat.getNumberOfElements(); ++i) {
        double x = mat[row][i / rows];
        cArray[i] = x;
        ++row;
        if (row == rows)
            row = 0;
    }
}

}

extern "C" _Bool isumi_stat_pca(
    const double *data,
    size_t variables,
    size_t observations,
    size_t num_components,
    double *coeff,
    double *score)
{
    std::unique_ptr<MATLABEngine> engine = matlab::engine::connectMATLAB();
    if (engine == nullptr)
        return false;
    ArrayFactory factory;
    std::vector<Array> args({
        factory.createArray<double>({observations, variables}, data, data + observations * variables),
        factory.createCharArray("NumComponents"),
        factory.createScalar(num_components)
    });

    std::vector<Array> results = engine->feval(u"pca", 2, args);
    assert(results.size() == 2);
    const Array &coeff_ = results[0];
    const Array &score_ = results[1];
    copyMatrixToCArray(coeff_, variables, coeff);
    copyMatrixToCArray(score_, observations, score);
    return true;
}
