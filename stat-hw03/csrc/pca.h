#pragma once

#ifdef __cplusplus
#include <cstdlib>
#include <cstdint>
#include <cstdbool>

extern "C" _Bool isumi_stat_pca(
        const double *data,
        size_t variables,
        size_t observations,
        size_t num_components,
        double *coeff,
        double *score
        );

namespace isumi::stat {

}

#endif
