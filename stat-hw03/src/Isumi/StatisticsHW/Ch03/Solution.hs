{-# LANGUAGE OverloadedStrings #-}

module Isumi.StatisticsHW.Ch03.Solution where

import           Isumi.StatisticsHW.Ch03.Compress
import           Options.Applicative
import           System.Environment
import           System.Exit
import           System.FilePath

main :: IO ()
main = do
  (Options inputPath compressionRatio) <- execParser opts
  let outputPath = takeDirectory inputPath </>
        takeBaseName inputPath <.> show compressionRatio <.> "bmp"
  imgM <- readRGB8 inputPath
  case imgM of
    Nothing -> putStrLn "Can not read file." >> exitFailure
    Just img -> 
      case compressImage img compressionRatio >>= decompressImage of
        Nothing -> putStrLn "Oops, error in compression or decompression"
                     >> exitFailure
        Just img' -> do
          saveRGB8 outputPath img'
          putStrLn $ "New image saved to " ++ outputPath ++ " successfully."

data Options = Options
  { filePath         :: FilePath
  , compressionRatio :: Double
  }

optionsParser :: Parser Options
optionsParser = Options
  <$> argument str (metavar "FILE")
  <*> option auto
      ( long "compression-ratio"
     <> metavar "RATIO"
     <> short 'r'
      )

opts = info (optionsParser <**> helper)
  ( fullDesc
 <> progDesc "Compress and reconstruct RGB8 image files"
 <> header "stat-hw03-exe - use pca to compress image"
  )
