{-# LANGUAGE FlexibleContexts #-}

module Isumi.StatisticsHW.Ch03.Compress
  ( readRGB8
  , saveRGB8
  , CompressedImage
  , compressImage
  , decompressImage
  ) where

import           Codec.Picture
import           Data.List                   (transpose)
import           Isumi.StatisticsHW.Ch03.PCA
import           Numeric.LinearAlgebra       (Matrix, Vector)
import qualified Numeric.LinearAlgebra       as LA
import qualified Numeric.LinearAlgebra.Data  as LA
import           Paths_stat_hw03
import           System.IO

readRGB8 :: FilePath -> IO (Maybe (Image PixelRGB8))
readRGB8 path = do
  dynImageE <- readImage path
  case dynImageE of
    Left err -> hPutStrLn stderr err >> pure Nothing
    Right (ImageRGB8 image) -> pure $ Just image
    Right _ -> hPutStrLn stderr "Not a RGB8 image" >> pure Nothing

saveRGB8 :: FilePath -> Image PixelRGB8 -> IO ()
saveRGB8 path image = saveBmpImage path (ImageRGB8 image)

newtype CompressedImage = CompressedImage [CompressedMat]

-- | Compress an RGB8 image
compressImage :: Image PixelRGB8
              -> Double -- ^compression ratio
              -> Maybe CompressedImage
compressImage pcas r = fmap CompressedImage
  . traverse (compressMatrix r) . rgb8ImageToMatrices $ pcas

-- | Decompress an RGB8 image
decompressImage :: CompressedImage -> Maybe (Image PixelRGB8)
decompressImage (CompressedImage cms) =
  matricesToRGB8Image =<< traverse decompressMatrix cms

data CompressedMat
  -- | (result of PCA, columns of original matrix)
  = CompressedMat PCAResult Int

compressMatrix :: Double -> Matrix Double -> Maybe CompressedMat
compressMatrix r m =
  if LA.rows m `rem` 16 /= 0 || LA.cols m `rem` 16 /= 0
     then Nothing
     else Just $
       let pcaInputMat = LA.fromColumns . fmap LA.fromList . transpose
             . fmap (concat . LA.toLists) . concat . LA.toBlocksEvery 16 16 $ m
        in CompressedMat (pca pcaInputMat (floor (256 / r))) (LA.cols m)

decompressMatrix :: CompressedMat -> Maybe (Matrix Double)
decompressMatrix (CompressedMat pcaResult cols) =
  if LA.cols pcaInputMat /= 256
     then Nothing
     else Just $
       LA.fromBlocks . groupN (cols `div` 16)
       . fmap (LA.reshape 16) . LA.toRows $ pcaInputMat
  where
  pcaInputMat = pcaInv pcaResult

rgb8ImageToMatrices :: Image PixelRGB8 -> [Matrix Double]
rgb8ImageToMatrices image = tuple3ToList . mapTuple3 LA.fromLists
  . unzip3 . fmap unzip3 $
    fmap (\r -> fmap (\c -> pixelAt' image c r) [0..width - 1]) [0..height - 1]
  where
  width = imageWidth image
  height = imageHeight image

matricesToRGB8Image :: [Matrix Double] -> Maybe (Image PixelRGB8)
matricesToRGB8Image [] = Just $ generateImage (\_ _ -> PixelRGB8 0 0 0) 0 0
matricesToRGB8Image [r, g, b] = Just $ generateImage gen width height
  where
  width = LA.cols r
  height = LA.rows r
  gen col row = tupleToRGB8 . mapTuple3 atIndexOf $ (r, g, b)
    where
    atIndexOf :: Matrix Double -> Double
    atIndexOf m = LA.atIndex m (row, col)
matricesToRGB8Image _ = Nothing

pixelAt' :: Image PixelRGB8 -> Int -> Int -> (Double, Double, Double)
pixelAt' image x y =
  let (PixelRGB8 r g b) = pixelAt image x y
   in mapTuple3 fromIntegral (r, g, b)

mapTuple3 :: (a -> b) -> (a, a, a) -> (b, b, b)
mapTuple3 f (x, y, z) = (f x, f y, f z)

tuple3ToList :: (a, a, a) -> [a]
tuple3ToList (a, b, c) = [a, b, c]

tupleToRGB8 :: (Double, Double, Double) -> PixelRGB8
tupleToRGB8 t =
  let (r, g, b) = mapTuple3 round t
   in PixelRGB8 r g b
