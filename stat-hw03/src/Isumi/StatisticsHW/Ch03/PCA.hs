{-# LANGUAGE OverloadedLists     #-}
{-# LANGUAGE ScopedTypeVariables #-}

module Isumi.StatisticsHW.Ch03.PCA
  ( PCAResult(..)
  , pca
  , pcaInv
  , groupN
  ) where

import qualified Data.ByteString            as BS
import           Data.Complex
import           Data.Foldable              (traverse_)
import           Foreign
import           Foreign.C
import           Isumi.Math.AppxEq
import qualified Numeric.LinearAlgebra      as LA
import           Numeric.LinearAlgebra.Data (Matrix, Vector)
import qualified Numeric.LinearAlgebra.Data as LA
import           System.IO.Unsafe           (unsafePerformIO)

pca :: Matrix Double -- ^observations
    -> Int -- ^number of components to reduce to
    -> PCAResult
pca m n = PCAResult coeff score
  (LA.fromList . fmap (mean . LA.toList) . LA.toColumns $ m)
  where
  (coeff, score) = pca' m n

data PCAResult = PCAResult
  { _pcaResult_coeff    :: Matrix Double
  , _pcaResult_score    :: Matrix Double
  , _pcaResult_inputAvg :: Vector Double
  } deriving Show

instance AppxEq PCAResult where
  PCAResult c1 s1 a1 ~= PCAResult c2 s2 a2 = c1 ~= c2 && s2 ~= s2 && a1 ~= a2

pcaInv :: PCAResult
       -> Matrix Double
-- score * coeff' + repmat(avgs, size(score, 1), 1)
pcaInv (PCAResult coeff score inputAvg) =
  (score <> LA.tr coeff) `LA.add`
    LA.fromRows (replicate (LA.rows score) inputAvg)

foreign import ccall unsafe "isumi_stat_pca"
  c_pca :: Ptr CDouble -- ^data
        -> CSize -- ^variables
        -> CSize -- ^observations
        -> CSize -- ^num components
        -> Ptr CDouble -- ^(result) coeff
        -> Ptr CDouble -- ^(result) score
        -> IO CBool

-- | Haskell wrapper over c_pca
{-# NOINLINE pca' #-}
pca' :: Matrix Double
     -> Int
     -> (Matrix Double, Matrix Double)
pca' mat n = unsafePerformIO $
  allocaArray (vars * obs) $ \(dataPtr :: Ptr CDouble) -> do
    let indicies = [(r, c) | c <- [0 .. vars - 1], r <- [0 .. obs - 1]]
    flip traverse_  indicies $ \(r, c) -> do
      let value = LA.atIndex mat (r, c)
      poke (plusPtr dataPtr (sizeOf (0.0 :: Double) * (c * obs + r))) value
    allocaArray (vars * n) $ \(coeffPtr :: Ptr CDouble) ->
      allocaArray (vars * obs) $ \(scorePtr :: Ptr CDouble) -> do
        _ <- c_pca dataPtr
               (fromIntegral vars)
               (fromIntegral obs)
               (fromIntegral n)
               coeffPtr scorePtr
        -- convert coeffPtr and scorePtr back
        coeffRaw <- peekArray (vars * n) coeffPtr
        scoreRaw <- peekArray (obs * n) scorePtr
        let coeff = columnMajorListToMatrix vars coeffRaw
            score = columnMajorListToMatrix obs scoreRaw
        pure (coeff, score)
  where
  vars = LA.cols mat
  obs = LA.rows mat

columnMajorListToMatrix :: Int -- ^rows
                        -> [CDouble] -- ^data
                        -> Matrix Double
columnMajorListToMatrix rows =
  LA.fromColumns . fmap LA.fromList . groupN rows . fmap unwrapCDouble

mean :: Fractional a => [a] -> a
mean xs = sum xs / fromIntegral (length xs)

groupN :: Int -> [a] -> [[a]]
groupN n [] = if n < 0 then repeat [] else []
groupN n xs =
  let (y, ys') = splitAt n xs
   in y : groupN n ys'

unwrapCDouble :: CDouble -> Double
unwrapCDouble (CDouble x) = x
