{-# LANGUAGE BangPatterns             #-}
{-# LANGUAGE DeriveGeneric            #-}
{-# LANGUAGE FlexibleContexts         #-}
{-# LANGUAGE ForeignFunctionInterface #-}
{-# LANGUAGE RankNTypes               #-}
{-# LANGUAGE ScopedTypeVariables      #-}
{-# LANGUAGE TypeOperators            #-}

module Isumi.StatisticsHW.Ch04.KMeans
  ( Point
  , clusterKMeans
  , clusterKMeansNative
  ) where

import           Control.DeepSeq             (NFData, force)
import           Control.Monad.Random.Class
import           Control.Monad.Random.Strict
import           Control.Monad.State.Strict  (MonadState (..), StateT,
                                              execStateT, get, put)
import           Control.Parallel.Strategies
import           Data.Foldable               (foldl')
import           Data.IntMap.Strict          (IntMap)
import qualified Data.IntMap.Strict          as IntMap
import           Data.List                   (find)
import           Data.Maybe                  (fromMaybe)
import qualified Data.Vector                 as V
import qualified Data.Vector.Unboxed         as UV
import           Foreign
import           Foreign.C
import           GHC.Generics                (Generic)
import           System.IO.Unsafe            (unsafePerformIO)
import           System.Random               (StdGen, mkStdGen)
import Debug.Trace

type Point = UV.Vector Double

{-|
  Use k-means method to create k clusters.

  Returns a vector of cluster centroids, along with a vector indicates
  to which cluster each point belongs (index correspond to input vector, value
  correspond to indicies inside the centroids vector)
-}
clusterKMeans :: V.Vector Point -- ^all points
              -> Int -- ^k
              -> Int -- ^max iteration limit
              -> (V.Vector Point, UV.Vector Int)
clusterKMeans !points !k !limit =
    let xs = iterate
               (\(c, _) -> clusterKMeansStep points c)
               (initialCentroids, UV.empty)
        ys = take limit (zip xs (tail xs))
     in fromMaybe
          (snd $ last ys)
          (snd <$> find (\(oldCs, newCs) -> fst oldCs == fst newCs) ys)
  where
    initialCentroids :: V.Vector Point
    initialCentroids =
      force $ evalRand (initClusters points k) (mkStdGen 2136802389)

clusterKMeansStep :: V.Vector Point -- ^all points
                  -> V.Vector Point -- ^current centroids
                  -> (V.Vector Point, UV.Vector Int) -- ^(next centroids, point index to centroid index)
clusterKMeansStep !points !centroids =
    let pointToCentroid :: V.Vector Int
        pointToCentroid =
            withStrategy (parVChunk 1000)
          . V.map (\p -> nearest p centroids)
          $ points
        centroidPSums :: IntMap PointSum
        centroidPSums =
          foldl' (IntMap.unionWith addPointSum) IntMap.empty
          . withStrategy (parList rseq)
          . fmap (V.foldl' (IntMap.unionWith addPointSum) IntMap.empty)
          . chunkV 1000
          . V.imap (\p c -> IntMap.singleton c (pointToSum $ points V.! p))
          $ pointToCentroid
        nextCentroids =
          V.generate k (\i -> fromMaybe (centroids V.! i) $!
            pointSumToCentroid <$> (centroidPSums IntMap.!? i))
     in (nextCentroids, V.convert pointToCentroid)
  where
    k = V.length centroids

parVChunk :: Int -> Strategy (V.Vector a)
parVChunk n v =
    V.concat <$> parList (evalTraversable rseq) (chunkV n v)

chunkV :: Int -> V.Vector a -> [V.Vector a]
chunkV n v
    | V.null v = []
    | otherwise =
        let (v', vs) = V.splitAt n v
         in v' : chunkV n vs

data PointSum = PointSum !Int !Point
  deriving (Show, Eq, Generic)

instance NFData PointSum

addPointSum :: PointSum -> PointSum -> PointSum
addPointSum (PointSum n1 p1) (PointSum n2 p2) =
    PointSum (n1 + n2) (UV.zipWith (+) p1 p2)

pointToSum :: Point -> PointSum
pointToSum p = PointSum 1 p

pointSumToCentroid :: PointSum -> Point
pointSumToCentroid (PointSum n p) = UV.map (/ fromIntegral n) p

nearest :: Point -> V.Vector Point -> Int
nearest p ps = V.minIndex (V.map (pointDistance p) ps)

-- | Computes distance of two points. Note, the distance mentioned here (and in
-- this module) is the square of Euclidean distance.
pointDistance :: Point -> Point -> Double
pointDistance x y = UV.sum $ UV.zipWith (\xi yi -> (xi - yi)^(2 :: Int)) x y

initClusters
  :: V.Vector Point -- ^A list of points. Each point is represent
  -> Int -- ^Number of clusters. The "k" in k-means
  -> Rand StdGen (V.Vector Point) -- ^initially selected centroids
initClusters points k = do
    -- first cluster centroid
    c0 <- uniform points
    let dist0 :: UV.Vector Double = UV.convert (fmap (pointDistance c0) points)
    r <- fmap fst . flip execStateT ([c0], dist0) $
           sequenceA (replicate (k - 1) step)
    pure $ V.fromList r
  where
    step :: StateT ([Point], UV.Vector Double) (Rand StdGen) ()
    step = do
      (cs, dists) <- get
      ci <- weighted (zip [0..] (fmap realToFrac (UV.toList dists)))
      let c = points V.! ci
      let dists' =
            UV.imap
              (\i d -> let d' = pointDistance (points V.! i) c in min d' d)
              dists
      put (c:cs, dists')

clusterKMeansNative :: V.Vector Point -- ^all points
                    -> Int -- ^k
                    -> Int -- ^max iteration limit
                    -> (V.Vector Point, UV.Vector Int)
clusterKMeansNative !points !k !limit = unsafePerformIO $ do
    withPoints points $ \c_points ->
      withPoints initialCentroids $ \c_initCents ->
        allocaArray (dim * k) $ \c_finalCents ->
          allocaArray count $ \c_pointCents -> do
            c_kmeans_cluster
              c_points
              (fromIntegral count)
              (fromIntegral dim)
              c_initCents
              (fromIntegral k)
              (fromIntegral limit)
              c_finalCents
              c_pointCents
            finalCentroids <- fromCPoints c_finalCents k dim
            pointCentroids <- UV.fromList <$>
              (fmap . fmap $ fromIntegral) (peekArray count c_pointCents)
            pure (finalCentroids, pointCentroids)
  where
    initialCentroids :: V.Vector Point
    initialCentroids =
      force $ evalRand (initClusters points k) (mkStdGen 2136802389)
    dim = UV.length (points V.! 0)
    count = V.length points
{-# NOINLINE clusterKMeansNative #-}

withPoints :: V.Vector Point -> (Ptr Double -> IO a) -> IO a
withPoints points action =
    allocaArray (count * dim) $ \arr -> do
      pokeArray arr (concatMap UV.toList $ V.toList points)
      action arr
  where
    dim = UV.length (points V.! 0)
    count = V.length points

fromCPoints :: Ptr Double -> Int -> Int -> IO (V.Vector Point)
fromCPoints c_points count dim = do
    values <- peekArray (count * dim) c_points
    pure . V.fromList $ fmap UV.fromList (chunk dim values)

chunk :: Int -> [a] -> [[a]]
chunk _ [] = []
chunk n xs =
    let (as, bs) = splitAt n xs
     in as : chunk n bs

foreign import ccall "kmeans_cluster"
  c_kmeans_cluster :: Ptr Double
                   -> CSize
                   -> CSize
                   -> Ptr Double
                   -> Word32
                   -> Word32
                   -> Ptr Double
                   -> Ptr Word32
                   -> IO ()
