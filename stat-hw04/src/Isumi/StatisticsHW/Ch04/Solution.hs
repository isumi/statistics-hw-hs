{-# LANGUAGE BangPatterns        #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TupleSections       #-}

module Isumi.StatisticsHW.Ch04.Solution
  ( main
  ) where

import           Control.DeepSeq                (force)
import qualified Data.ByteString.Lazy           as BS
import           Data.Function                  (on)
import           Data.Int                       (Int64)
import           Data.IntMap.Strict             (IntMap)
import qualified Data.IntMap.Strict             as IntMap
import           Data.List                      (maximumBy)
import qualified Data.Vector                    as V
import qualified Data.Vector.Unboxed            as UV
import           Data.Word                      (Word8)
import           GHC.Exts                       (fromList)
import           Isumi.StatisticsHW.Ch04.KMeans
import           Isumi.StatisticsHW.Ch04.Mnist  (readImages, readLabels)
import           Numeric.LinearAlgebra          (Matrix)
import qualified Numeric.LinearAlgebra          as LA
import           System.Environment             (getArgs)
import           System.Exit                    (exitFailure)
import           System.IO                      (hPutStrLn, stderr)

main :: IO ()
main = do
    putStrLn "Started"
    [method, limit, fpImages, fpLabels] <- getArgs
    let cluster = if method == "c" then clusterKMeansNative else clusterKMeans
    (images, labels) <- readImageAndLabelFromFile fpImages fpLabels
    let (_, !piToCi) = cluster images 10 (read limit)
        !derivedLabels = deriveClusterLabels labels piToCi
        matchRatio :: Double =
          fromIntegral (UV.length . UV.filter id $
            UV.zipWith (==) labels derivedLabels)
          / fromIntegral (UV.length labels)
    print matchRatio

readImageAndLabelFromFile :: FilePath -- ^filepath of images
                          -> FilePath -- ^filepath of labels
                          -> IO (V.Vector Point, UV.Vector Word8)
readImageAndLabelFromFile fpImages fpLabels = do
    imagesM <- force . readImages <$> BS.readFile fpImages
    labelsM <- force . readLabels <$> BS.readFile fpLabels
    case (,) <$> ((fmap . fmap) imageAsPoint imagesM) <*> labelsM of
      Nothing -> exitFailureWith "Failed to read mnist data files."
      Just r  -> pure r

exitFailureWith :: String -> IO a
exitFailureWith msg = do
    hPutStrLn stderr msg
    exitFailure

imageAsPoint :: Matrix Int64 -> Point
imageAsPoint mat = fromList . fmap fromIntegral . concat . LA.toLists $ mat

deriveClusterLabels :: UV.Vector Word8 -- ^real labels
                    -> UV.Vector Int -- ^point index to centroid index
                    -> UV.Vector Word8 -- ^derived labels
deriveClusterLabels labels piToCi =
    let cs = UV.ifoldl' step IntMap.empty piToCi
        centroidLabels = fmap
          (\c -> fromIntegral . maxOnSnd . IntMap.toList $ c)
          cs
     in UV.map (\c -> centroidLabels IntMap.! c) piToCi
  where
    step :: IntMap (IntMap Int) -> Int -> Int -> IntMap (IntMap Int)
    step cls p c =
      let realLabel = fromIntegral $ labels UV.! p
       in if IntMap.notMember c cls
          then IntMap.insert c (IntMap.fromList [(realLabel, 1)]) cls
          else IntMap.insert c
                 (IntMap.insertWith (+) realLabel 1 (cls IntMap.! c)) cls

maxOnSnd :: Ord b => [(a, b)] -> a
maxOnSnd xs = fst $ maximumBy (compare `on` snd) xs
