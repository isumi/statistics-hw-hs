{-# LANGUAGE ScopedTypeVariables #-}

module Isumi.StatisticsHW.Ch04.Mnist
  ( readImages
  , readLabels
  ) where

import           Data.Bifunctor        (first)
import           Data.Bits
import           Data.ByteString.Lazy  (ByteString)
import qualified Data.ByteString.Lazy  as ByteString
import           Data.Foldable         (foldl')
import           Data.Int              (Int64)
import qualified Data.Vector           as V
import qualified Data.Vector.Unboxed   as UV
import           Data.Word             (Word32, Word8)
import           GHC.Exts              (fromList)
import           Numeric.LinearAlgebra (Matrix)
import qualified Numeric.LinearAlgebra as LA

readImages :: ByteString -> Maybe (V.Vector (Matrix Int64))
readImages bs =
    if length headers /= 4 || magic /= 0x803
       || fromIntegral (nImages * rows * cols) /= ByteString.length pixels
    then Nothing
    else Just . V.fromList $
      fmap
        ( LA.reshape (fromIntegral cols)
        . fromList . fmap fromIntegral . ByteString.unpack
        )
        ( splitEveryN (rows * cols) pixels )
  where
    (headers, pixels) = first (splitEveryN (4 :: Int)) (ByteString.splitAt 16 bs)
    [magic, nImages, rows, cols] = fmap readWord32BE headers

readLabels :: ByteString -> Maybe (UV.Vector Word8)
readLabels bs =
    if magic /= 0x801 || (ByteString.length bs - 8) /= fromIntegral n
    then Nothing
    else Just . UV.fromList . ByteString.unpack $ labels
  where
    (magic, bs') = first readWord32BE (ByteString.splitAt 4 bs)
    (n, labels) = first readWord32BE (ByteString.splitAt 4 bs')

readWord32BE :: ByteString -> Word32
readWord32BE bs =
    foldl' (\y x -> shiftL y 8 `xor` x) 0 (fmap fromIntegral bytes)
  where
    bytes = ByteString.unpack . ByteString.take 4 $ bs

splitEveryN :: Integral a => a -> ByteString -> [ByteString]
splitEveryN n bs =
    if ByteString.null bs
    then []
    else let (x, remains) = ByteString.splitAt (fromIntegral n) bs
          in x : splitEveryN n remains

