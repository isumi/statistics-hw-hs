#include "kmeans.h"
#include <math.h>
#include <stdbool.h>
#include <stdlib.h>
#include <float.h>
#include <string.h>
#include <stdio.h>

static double point_distance(const double *p1, const double *p2, size_t dim);

static uint32_t nearest_centroid(
    const double *p,
    size_t dim,
    const double *centroids,
    uint32_t k);

struct PointSum
{
  size_t count;
  double *point;
};

static void init_pointSums(struct PointSum *pointSums, size_t n, size_t dim);

void kmeans_cluster(
    const double *points,
    size_t count,
    size_t dimension,
    const double *initialCentroids,
    uint32_t k,               // number of clusters/centroids
    uint32_t limit,           // max number of iterations
    double *centroids,        // (output) the final centroids
    uint32_t *pointCentroids) // (output) point index to centroid index
{
  memcpy(centroids, initialCentroids, k * dimension * sizeof(double));
  struct PointSum *centroidPSums = malloc(k * sizeof(struct PointSum));
  for (size_t i = 0; i != k; ++i)
  {
    centroidPSums[i].point = malloc(dimension * sizeof(double));
  }
  bool changed = true;
  while (changed && limit > 0)
  {
    changed = false;
    init_pointSums(centroidPSums, k, dimension);
    for (size_t pid = 0; pid != count; ++pid)
    {
      const double *p = points + pid * dimension;
      uint32_t cid = nearest_centroid(p, dimension, centroids, k);
      pointCentroids[pid] = cid;

      centroidPSums[cid].count++;
      double *psum = centroidPSums[cid].point;
      for (size_t i = 0; i != dimension; ++i)
      {
        psum[i] += p[i];
      }
    }
    for (size_t i = 0; i != k; ++i)
    {
      size_t count = centroidPSums[i].count;
      double *newCent = centroidPSums[i].point;
      for (size_t j = 0; j != dimension; ++j)
      {
        newCent[j] /= count;
      }
      double *oldCent = centroids + i * dimension;
      if (memcmp(oldCent, newCent, dimension * sizeof(double)) != 0)
      {
        changed = true;
      }
      memcpy(oldCent, newCent, dimension * sizeof(double));
    }
    --limit;
  }

  free(centroidPSums);
  for (size_t i = 0; i != k; ++i)
  {
    free(centroidPSums[i].point);
  }
}

static void init_pointSums(struct PointSum *pointSums, size_t n, size_t dim)
{
  for (size_t i = 0; i != n; ++i)
  {
    pointSums[i].count = 0;
    double *point = pointSums[i].point;
    for (size_t j = 0; j != dim; ++j)
    {
      point[j] = 0;
    }
  }
}

static double point_distance(const double *p1, const double *p2, size_t dim)
{
  double sum = 0;
  for (size_t i = 0; i != dim; ++i)
  {
    double diff = fabs(p1[i] - p2[i]);
    sum += diff * diff;
  }
  return sum;
}

static uint32_t nearest_centroid(
    const double *p,
    size_t dim,
    const double *centroids,
    uint32_t k)
{
  double min_dist = DBL_MAX;
  uint32_t cid = 0;
  for (uint32_t i = 0; i != k; ++i)
  {
    double dist = point_distance(p, centroids + dim * i, dim);
    if (dist < min_dist)
    {
      cid = i;
      min_dist = dist;
    }
  }
  return cid;
}