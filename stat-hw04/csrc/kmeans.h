#include <stdlib.h>
#include <stdint.h>

void kmeans_cluster(
    const double *points,
    size_t count,
    size_t dimension,
    const double *initialCentroids,
    uint32_t k,                // number of clusters/centroids
    uint32_t limit,            // max number of iterations
    double *centroids,         // (output) the final centroids
    uint32_t *pointCentroids); // (output) point index to centroid index
